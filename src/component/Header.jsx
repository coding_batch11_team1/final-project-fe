import * as React from 'react';
import PropTypes from 'prop-types';
import AppBar from '@mui/material/AppBar';
import Box from '@mui/material/Box';
import CssBaseline from '@mui/material/CssBaseline';
import Divider from '@mui/material/Divider';
import Drawer from '@mui/material/Drawer';
import IconButton from '@mui/material/IconButton';
import List from '@mui/material/List';
import ListItem from '@mui/material/ListItem';
import ListItemButton from '@mui/material/ListItemButton';
import ListItemText from '@mui/material/ListItemText';
import MenuIcon from '@mui/icons-material/Menu';
import Toolbar from '@mui/material/Toolbar';
import Typography from '@mui/material/Typography';
import Button from '@mui/material/Button';
import OtomobilLogo from '../assets/otomobilLogo.svg'
import customColors from "./Colors";
import { Link, NavLink } from 'react-router-dom';

const drawerWidth = 240;

export default function Header(props) {
  const { window } = props;
  const [mobileOpen, setMobileOpen] = React.useState(false);

  const handleDrawerToggle = () => {
    setMobileOpen((prevState) => !prevState);
  };

  const drawer = (
    <Box onClick={handleDrawerToggle} sx={{ textAlign: 'center' }}>
      <img src={OtomobilLogo} alt="Otomobil Logo" style={{ marginBottom: '-30px' }} />
      <Typography variant="h6" sx={{ my: 1 }}>
        Otomobil
      </Typography>
      <Divider />
      <List>
        <NavLink to="/login">
                    <ListItem disablePadding>
                        <ListItemButton sx={{ textAlign: 'center' }}>
                            <ListItemText primary="Login" />
                        </ListItemButton>
                    </ListItem>
                </NavLink>
                <NavLink to="/register">
                    <ListItem disablePadding>
                        <ListItemButton sx={{ textAlign: 'center' }}>
                            <ListItemText primary="Sign Up" />
                        </ListItemButton>
                    </ListItem>
                </NavLink>
      </List>
    </Box>
  );

  const container = window !== undefined ? () => window().document.body : undefined;
  const loginColor = customColors.palette.ochre.loginColor;

  return (
    <Box sx={{ display: 'flex' }}>
      <CssBaseline />
      <AppBar component="nav" sx={{backgroundColor: 'white', color: 'black', boxShadow: 'none'}}>
        <Toolbar>
          <IconButton
            color="inherit"
            aria-label="open drawer"
            edge="start"
            onClick={handleDrawerToggle}
            sx={{ mr: 2, display: { sm: 'none' } }}
          >
            <MenuIcon />
          </IconButton>
          <Typography
            variant="h6"
            component="div"
            sx={{ 
                flexGrow: 1, 
                display: { xs: 'none', sm: 'flex' },  
                alignItems: 'center', 
                gap: 2,
                fontSize:'24px'
            }}>
            <Link to={'/'}>
            <img src={OtomobilLogo} alt="Otomobil Logo" width={62.4} height={50} style={{ verticalAlign: 'middle', marginLeft: '20px', marginRight: '-15px'}} />
            </Link>
            Otomobil
          </Typography>

          <Box sx={{ display: { xs: 'none', sm: 'block' } }}>
            <NavLink to="/register">
              <Button sx={{ color: loginColor, marginRight: '20px', fontSize:'16px', }}>
                Sign Up
              </Button>
            </NavLink>
            <NavLink to="/login">
              <Button sx={{ color: 'white',
                backgroundColor: loginColor,
                borderRadius: '10px',
                marginRight: '20px',
                fontSize:'16px',
                '&:hover': {
                  color: 'white',
                  backgroundColor: '#6a0008',
                } 
              }}>
                Login
              </Button>
            </NavLink>
          </Box>
        </Toolbar>
      </AppBar>
      <nav>
        <Drawer
          container={container}
          variant="temporary"
          open={mobileOpen}
          onClose={handleDrawerToggle}
          ModalProps={{
            keepMounted: true,
          }}
          sx={{
            display: { xs: 'block', sm: 'none' },
            '& .MuiDrawer-paper': { boxSizing: 'border-box', width: drawerWidth },
          }}
        >
          {drawer}
        </Drawer>
      </nav>
    </Box>
  );
}

Header.propTypes = {
  /**
   * Injected by the documentation to work in an iframe.
   * You won't need it on your project.
   */
  window: PropTypes.func,
};
