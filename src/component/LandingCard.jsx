import { Box, Typography, Grid, Avatar } from '@mui/material';
import Header from './Header';

export default function LandingCard() {

  return (
    <>
    <Box
        display='flex'
        alignItems="center" 
        justifyContent="center"
        flexDirection='column' 
        style={{
            marginTop: '4rem',
        }} 
      sx={{
        backgroundImage: 'url(/backgroundImage.png)',
        // backgroundColor: 'black',
        backgroundSize: 'cover',
        backgroundPosition: 'center',
        height: '496px',  // or set a specific height
        color: '#fff' // white text color
      }}
    >
      <Typography textAlign='center' variant="h3">We provide driving lessons for various types of cars</Typography>
      <Typography textAlign='center' variant="h6">Professional staff who are ready to help you to become a much-needed reliable driver</Typography>
      
      <Grid container spacing={3} alignItems='center' justifyContent='center'>
        <Grid item xs={3} 
          sx={{
            margin: 2,
            textAlign: 'center'
          }}
        >
          <Typography variant="h4">50+</Typography>
          <Typography>A class ready to make you a reliable driver</Typography>
        </Grid>
        <Grid item xs={3}
          sx={{
            margin: 2,
            textAlign: 'center'
          }}
        >
          <Typography variant="h4">20+</Typography>
          <Typography>Professional workforce with great experience</Typography>
        </Grid>
        <Grid item xs={3}
          sx={{
            margin: 2,
            textAlign: 'center'
          }}
        >
          <Typography variant="h4">10+</Typography>
          <Typography>Cooperate with driver service partners</Typography>
        </Grid>
      </Grid>
    </Box>
    </>
  );
}
