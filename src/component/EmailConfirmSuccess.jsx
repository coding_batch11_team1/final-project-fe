import React from 'react';
import { Button, TextField, Container, Typography, Grid, Box } from '@mui/material';
import GambarEmailSuccess from '../assets/GambarEmailConfirmSuccess.png'
import { Link } from 'react-router-dom';
import customColors from './Colors';
import { color } from '@mui/system';


function EmailConfirmSuccess() {
  const loginColor = customColors.palette.ochre.loginColor;
    return (
        <Box
        display="flex" 
        alignItems="center" 
        justifyContent="center" 
        style={{ height: '100vh' }}
        >
          <Grid container direction="column" spacing={3} alignItems="center">

            <Grid item>
              <img src={GambarEmailSuccess} alt="Email Confirm Success" style={{ maxWidth: '200px', height: 'auto' }} />
            </Grid>
    
            <Grid item>
              <Typography variant='h4' align='center'>
                Email Confirmation Success
              </Typography>
            </Grid>
    
            <Grid item>
              <Typography align='center'>
                Your email already! Please login first to access the web
              </Typography>
            </Grid>
    
            <Grid item>
              <Link to={'/login'}>
              <Button variant='contained' style={{backgroundColor: loginColor}}>
                Login Here
              </Button>
              </Link>
            </Grid>
          </Grid>
        </Box>
      );
}

export default EmailConfirmSuccess;
