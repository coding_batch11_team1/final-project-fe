import { Box, Container, Divider, Grid, Typography } from "@mui/material";
import Stack from '@mui/material/Stack';

export default function Footer() {
    return (
        <Box
        display='flex'
        flexDirection='column'
        alignItems='start'
        justifyContent='center'
        mt={3}
        mb={5}
        >
            <Container maxWidth="lg">
                <Grid container spacing={5}>
                    <Grid item md={4}>
                        <Typography variant="h5" gutterBottom>About Us</Typography>
                        <Typography>Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo. </Typography>
                    </Grid>
                    <Grid item md={4}>
                        <Typography variant="h5" >Product</Typography>
                        <Grid container>
                            <Grid item xs={6}>
                            <ul>
                                <li>Electric</li>
                                <li>LCGC</li>
                                <li>Offroad</li>
                                <li>SUV</li>
                            </ul>
                            </Grid>
                            <Grid item xs={6}>
                                <ul>
                                    <li>Hatchback</li>
                                    <li>MPV</li>
                                    <li>Sedan</li>
                                    <li>Truck</li>
                                </ul>
                            </Grid>
                        </Grid>
                    </Grid>
                    <Grid item md={4}>
                        <Typography variant="h5" gutterBottom>Address</Typography>
                        <Typography mb={2}>Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque.</Typography>
                        <Typography variant="h5" gutterBottom>Contact Us</Typography>
                        <Stack flexDirection='row' gap={2}>
                            <img src="/icon/phone.png" width='40px' />
                            <img src="/icon/instagram.png" width='40px' />
                            <img src="/icon/youtube.png" width='40px' />
                            <img src="/icon/telegram.png" width='40px' />
                            <img src="/icon/message.png" width='40px' />
                        </Stack>
                    </Grid>
                </Grid>
            </Container>
        </Box>
      )
    
}