import { useState } from 'react';
import { Button, TextField, Typography, Container, Grid, Box } from '@mui/material';
import customColors from "./Colors";
import { Link } from 'react-router-dom';

export default function ResetPassForm() {
    // State for the email input
    const loginColor = customColors.palette.ochre.loginColor;
    const [email, setEmail] = useState(null);

    const handleEmailChange = (e) => {
        setEmail(e.target.value);
        console.log(email)
    };

    const validateEmail = async () => {
        await delay(1000)
    }

    const submitConfirm = async (e) => {
        e.preventDefault()

        console.log("login start")
        setLoading(true)
        await validateEmail()
        await login()
        setLoading(false)
        console.log("login end")
    };

    return (
        <Box
        display="flex" 
        alignItems="center" 
        justifyContent="center" 
        style={{ height: '80vh' }}
        >
        <Container maxWidth='md'>            
            <Typography variant="h5" align='left' mb={2}>
                Reset Password
            </Typography>
            <Typography align='left' mb={6}>
                Send OTP code to your email address
            </Typography>
            <form onSubmit={submitConfirm}>
                <Grid container spacing={3}>
                    <Grid item xs={12} sm={12}>
                        <TextField
                            fullWidth
                            size='small'
                            label="Email"
                            variant="outlined"
                            name="email"
                            value={email}
                            onChange={handleEmailChange}
                        />
                    </Grid>
                    <Grid item xs={12}>
                        <Grid container spacing={2} justifyContent="flex-end">
                            <Grid item xs={12} sm={3} md={2} lg={2}>
                                <Link to={'/login'}> 
                                <Button
                                    variant="outlined"
                                    style={{color: loginColor, padding: '7px 40px', borderRadius: '10px', borderColor: loginColor}}
                                    sx={{ width: '100%' }}
                                >
                                    Cancel   
                                </Button>
                                </Link>
                            </Grid>
                            <Grid item xs={12} sm={3} md={2} lg={2}>
                                <Link to={'/newPass'} >
                                <Button
                                    type="submit"
                                    variant="contained"
                                    style={{backgroundColor: loginColor, padding: '7px 40px', borderRadius: '10px'}}
                                    sx={{ width: '100%' }}
                                >
                                    Confirm
                                </Button>
                                </Link>
                            </Grid>
                        </Grid>
                    </Grid>
                </Grid>
            </form>
        </Container>
        </Box>
    );
}
