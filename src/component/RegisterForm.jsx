import { useState } from 'react';
import customColors from "./Colors";
import { Button, TextField, Container, Typography, Grid, Box } from '@mui/material';
import { Link } from 'react-router-dom';

export default function RegisterForm() {
    const loginColor = customColors.palette.ochre.loginColor;
    const [name, setName] = useState(null)
    const [email, setEmail] = useState(null)
    const [password, setPassword] = useState(null)
    const [confirmPassword, setConfirmPassword] = useState(null)

    const handleSubmit = (e) => {
        e.preventDefault();
    };

    return (
        <Box
        display="flex" 
        alignItems="center" 
        justifyContent="center" 
        style={{ height: '100vh' }}
        >
        <Container maxWidth='md'>
            <Typography variant="h4" align="left">
                Let's Join our course!
            </Typography>
            <Typography variant="h5" align="left" mb={6} mt={2}>
                Please register first
            </Typography>
            <form onSubmit={handleSubmit}>
                <Grid container spacing={3}>
                    <Grid item xs={12} sm={12}>
                        <TextField
                            fullWidth
                            size='small'
                            label="Name"
                            variant="outlined"
                            name="name"
                            value={name}
                            onChange={e => setName(e.target.value)}
                        />
                    </Grid>
                    <Grid item xs={12} sm={12}>
                        <TextField
                            fullWidth
                            size='small'
                            label="Email"
                            variant="outlined"
                            name="email"
                            value={email}
                            onChange={e => setEmail(e.target.value)}
                        />
                    </Grid>
                    <Grid item xs={12} sm={12}>
                        <TextField
                            fullWidth
                            size='small'
                            label="Password"
                            variant="outlined"
                            type="password"
                            name="password"
                            value={password}
                            onChange={e => setPassword(e.target.value)}
                        />
                    </Grid>
                    <Grid item xs={12} sm={12}>
                        <TextField
                            fullWidth
                            size='small'
                            label="Confirm Password"
                            variant="outlined"
                            type="password"
                            name="confirmPassword"
                            value={confirmPassword}
                            onChange={e => setConfirmPassword(e.target.value)}
                        />
                    </Grid>
                    <Grid item xs={12}>
                        <Box textAlign="right" mt={2}>
                            <Link to={'/emailConfirmSuccess'}>
                            <Button
                                type="submit"
                                variant="contained"
                                style={{backgroundColor: loginColor, padding: '7px 40px', borderRadius: '10px'}}
                            >
                                Sign Up
                            </Button>
                            </Link>
                        </Box>
                    </Grid>
                </Grid>
            </form>
            <Typography align="center" mt={12}>
                Have an account? <Link to={'/login'}>Login here</Link>
            </Typography>
        </Container>
        </Box>
    );
}