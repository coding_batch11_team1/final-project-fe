import { useState } from 'react';
import customColors from "./Colors";
import { Button, TextField, Container, Typography, Grid, Box } from '@mui/material';
import { Link } from 'react-router-dom';

export default function LoginForm() {
    const loginColor = customColors.palette.ochre.loginColor;
    const [newPassword, setNewPassword] = useState(null)
    const [confirmNewPassword, setConfirmNewPassword] = useState(null)

    const handleSubmit = (e) => {
        e.preventDefault();
    };

    return (
        <Box
        display="flex" 
        alignItems="center" 
        justifyContent="center" 
        style={{ height: '100vh' }}
        >
        <Container maxWidth='md'>
            <Typography variant="h4" align="left" mb={6}>
                Create Password
            </Typography>
            <form onSubmit={handleSubmit}>
                <Grid container spacing={3}>
                    <Grid item xs={12} sm={12}>
                        <TextField
                            fullWidth
                            size='small'
                            label="New Password"
                            variant="outlined"
                            name="newPassword"
                            value={newPassword}
                            onChange={e => setNewPassword(e.target.value)}
                        />
                    </Grid>
                    <Grid item xs={12} sm={12}>
                        <TextField
                            fullWidth
                            size='small'
                            label="Confirm New Password"
                            variant="outlined"
                            type="password"
                            name="confirmNewPasswordassword"
                            value={confirmNewPassword}
                            onChange={e => setConfirmNewPassword(e.target.value)}
                        />
                    </Grid>
                    <Grid item xs={12}>
                    <Grid container spacing={2} justifyContent="flex-end">
                            <Grid item xs={12} sm={3} md={2} lg={2}>
                                <Button
                                    variant="outlined"
                                    style={{color: loginColor, padding: '7px 40px', borderRadius: '10px', borderColor: loginColor}}
                                    sx={{ width: '100%' }}
                                >
                                    Cancel   
                                </Button>
                            </Grid>
                            <Grid item xs={12} sm={3} md={2} lg={2}>
                                <Link to={'/login'} >
                                <Button
                                    type="submit"
                                    variant="contained"
                                    style={{backgroundColor: loginColor, padding: '7px 40px', borderRadius: '10px'}}
                                    sx={{ width: '100%' }}
                                >
                                    Submit
                                </Button>
                                </Link>
                            </Grid>
                        </Grid>
                    </Grid>
                </Grid>
            </form>
        </Container>
        </Box>
    );
}