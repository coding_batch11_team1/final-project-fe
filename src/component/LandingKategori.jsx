import { Box, Container, Grid, Typography } from "@mui/material";


export default function LandingKategori() {
    return (
        <Box
        display='flex'
        flexDirection='column'
        alignItems='center'
        mb={10}
        >
            <Container maxWidth='md'>
                <Typography variant="h4" textAlign='center' mt={7} mb={7}>
                    More car you can choose
                </Typography>
                <Grid container spacing={5} justifyContent='center'>
                    <Grid item>
                        <img src="https://placehold.co/100x100" />
                        <Typography textAlign='center'>MPV</Typography>
                    </Grid>
                    <Grid item>
                        <img src="https://placehold.co/100x100" />
                        <Typography textAlign='center'>MPV</Typography>
                    </Grid>
                    <Grid item>
                        <img src="https://placehold.co/100x100" />
                        <Typography textAlign='center'>MPV</Typography>
                    </Grid>
                    <Grid item>
                        <img src="https://placehold.co/100x100" />
                        <Typography textAlign='center'>MPV</Typography>
                    </Grid>
                </Grid>
            </Container>
        </Box>
    )
}