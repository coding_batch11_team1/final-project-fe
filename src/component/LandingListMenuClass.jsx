import * as React from 'react';
import Card from '@mui/material/Card';
import CardContent from '@mui/material/CardContent';
import CardMedia from '@mui/material/CardMedia';
import Typography from '@mui/material/Typography';
import { Box, CardActionArea } from '@mui/material';
import { Fullscreen } from '@mui/icons-material';

export default function LandingListMenuClass() {
  return (
    <Card sx={{ maxWidth: '100%', maxHeight: '100%', marginTop: '4rem' }}>
      <CardActionArea>
        <CardMedia
          component="img"
          image="/LandingListMenuClass.png"
          alt="green iguana"
        />
        <Box 
        display='flex'
        alignContent='center'
        justifyContent='center'
        >
        <CardContent sx={{maxWidth: 'lg'}}>
          <Typography gutterBottom variant="h5" component="div">
          SUV
          </Typography>
          <Typography variant="body2" color="text.secondary">
          Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.
          </Typography>
        </CardContent>
        </Box>
      </CardActionArea>
    </Card>
  );
}