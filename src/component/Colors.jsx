const customColors = {
  palette: {
    ochre: {
      main: '#E3D026',
      light: '#E9DB5D',
      dark: '#A29415',
      contrastText: '#242105',
      loginColor: '#790B0A'
    },
  },
};

export default customColors