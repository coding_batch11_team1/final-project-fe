import AppBar from '@mui/material/AppBar';
import Typography from '@mui/material/Typography';
import OtomobilLogo from '../assets/otomobilLogo.svg'

function HeaderEmailConfirmSuccess() {
  return (
    <AppBar component="nav" sx={{backgroundColor: 'white', color: 'black', boxShadow: 'none', height: '100px'}}>
          <Typography
            variant="h6"
            component="div"
            sx={{  
                display: 'flex',  
                alignItems: 'center', 
                gap: 2,
                fontSize:'24px'
            }}>
            <img src={OtomobilLogo} alt="Otomobil Logo" width={62.4} height={50} style={{ verticalAlign: 'middle', marginLeft: '20px', marginRight: '-15px'}} />
            Otomobil
          </Typography>
      </AppBar>
  );
}

export default HeaderEmailConfirmSuccess;
