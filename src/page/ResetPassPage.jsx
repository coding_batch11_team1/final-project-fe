import ResetPassForm from "../component/ResetPassForm"
import Header from '../component/Header'

export default function ResetPassPage() {
    return (
        <>
            <Header />
            <ResetPassForm />
        </>
    )
}