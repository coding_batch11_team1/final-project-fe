import Header from '../component/Header'
import RegisterForm from '../component/RegisterForm'

function RegisterFormPage() {

  return (
    <>
      <Header />
      <RegisterForm />
    </>
  )
}

export default RegisterFormPage;
