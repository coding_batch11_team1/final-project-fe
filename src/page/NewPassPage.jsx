import Header from "../component/Header";
import NewPass from "../component/NewPassForm";


export default function NewPassPage() {
    return (
        <>
            <Header />
            <NewPass />
        </>
    )
}