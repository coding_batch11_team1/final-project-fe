import Header from "../component/Header";
import LandingCard from '../component/LandingCard'
import LandingBenefit from "../component/LandingBenefit";
import LandingList from "../component/LandingList";
import LandingKategori from "../component/LandingKategori";
import Footer from "../component/Footer";
import { Divider } from "@mui/material";
import LandingHeader from "../component/LandingHeader";

export default function LandingPage() {
    return (
        <>
            <LandingHeader />     
            <LandingCard />   
            <LandingList />
            <LandingBenefit />
            <LandingKategori />
            <Divider />
            <Footer />
        </>
    )
}