import LandingList from "../component/LandingList";
import Footer from "../component/Footer";
import { Divider } from "@mui/material";
import LandingHeader from "../component/LandingHeader";
import LandingListMenuClass from "../component/LandingListMenuClass";

export default function LandingListMenuClassPage() {
    return (
        <>
            <LandingHeader /> 
            <LandingListMenuClass />
            <Divider />    
            <LandingList />
            <Divider />
            <Footer />
        </>
    )
}