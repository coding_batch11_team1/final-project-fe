import Header from "../component/Header";
import LoginForm from '../component/LoginForm'


export default function loginPage() {
    return (
        <>
            <Header />
            <LoginForm />
        </>
    )
}