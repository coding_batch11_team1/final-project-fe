
import { BrowserRouter, Route, Routes } from "react-router-dom";
import RegisterPage from './page/RegisterPage'
import Header from './component/Header'
import EmailConfirmSuccessPage from './page/EmailConfirmSuccessPage'
import loginPage from "./page/LoginPage";
import ResetPassPage from './page/ResetPassPage';
import NewPassPage from './page/NewPassPage';
import LandingPage from "./page/LandingPage";
import LandingPageAfterLogin from './page/LandingPageAfterLogin'
import LandingListMenuClassPage from "./page/LandingListMenuClassPage";

function App() {

  return (
    <BrowserRouter>
        <Header />
        <Routes>
          <Route path="/" Component={LandingPageAfterLogin} />
          <Route path="/register" Component={RegisterPage} />
          <Route path="/login" Component={loginPage} />
          <Route path="/emailConfirmSuccess" Component={EmailConfirmSuccessPage} />
          <Route path='/resetPassword' Component={ResetPassPage} />
          <Route path="/newPass" Component={NewPassPage} />
        </Routes>
    </BrowserRouter>
  )
}

export default App
